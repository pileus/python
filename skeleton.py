#!/usr/bin/env python
# -*- coding: utf-8 -*-


def main():
    """The main function."""
    pass


if __name__ == "__main__":
    # main function call only if the current module
    # is not imported
    main()
