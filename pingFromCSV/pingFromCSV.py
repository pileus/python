#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script qui permet de parser un export csv de Zeroshell_Franklin_Network_Routeur.ods, c'est encore très sale
mais fonctionnel.
Le programme créer un fichier resultat.csv à l'endroit où est lancé le script.

Projets d'amélioration :
    - Parser directement le fichier ods
    - Écrire directement un fichier ods (coloration verte si UP)
    - Faire les pings en multithreading
"""


import csv  # Module pour traiter un fichier csv
# import socket # Pour traiter les pings en multithreading
import os

from time import localtime, strftime


def getmoment():
    """
    :return l'heure à laquelle l'action se passe
    """
    return strftime("%d/%m/%Y %H:%M", localtime())


def readcsvfile(fichier):
    """
    Ouvre un fichier CSV, Comma Separated Values et retourne un objet DictReader
    :param fichier:
    :return:
    """
    fichier_csv = open(fichier, newline='')
    return csv.DictReader(fichier_csv)


def doping(ip_addr):
    """
    Demande au système de faire un ping IPv4 en redirigeant le résultat dans /dev/null. >> Unix only
    Si le ping abouti la machine est considérée vivante, retourne True.
    :param ip_addr:
    :return:
    """
    if os.system("ping -4 -c 3 " + ip_addr + '> /dev/null') == 0:
        return True
    else:
        return False


def main():
    """The main function."""
    fichier_lu = input("Renseigner le fichier source au format csv :")
    fichier_ecrit = fichier_lu.split(".")[0] + "_isAlive-" + strftime("%Y-%m-%d", localtime()) + ".csv"
    lire = readcsvfile(fichier_lu)
    os.system("echo 'Fixed IP,MAC Address,Description,État au " + getmoment() + "' > " + fichier_ecrit)
    for row in lire:
        if doping(row['Fixed IP'].strip()):
            os.system("echo '" + row['Fixed IP'].strip() + "," + row['MAC Address'].strip() + "," + row[
                '\xa0\xa0Description'].strip() + ",1' >> " + fichier_ecrit)
        else:
            os.system("echo '" + row['Fixed IP'].strip() + "," + row['MAC Address'].strip() + "," + row[
                '\xa0\xa0Description'].strip() + "," + "0' >> " + fichier_ecrit)


if __name__ == "__main__":
    # main function call only if the current module
    # is not imported
    main()
