import socket #Interface BSD Sockets pour faire le test de connexion
import time #Pour avoir une référence de temps
import threading #Constructs higher-level threading interfaces on top of the lower level
from queue import Queue #Importe la classe Queue du module queue pour avoir une pile FIFO

socket.setdefaulttimeout(0.25) #Permet de lever une erreur lorsque le temps est dépassé


print_lock = threading.Lock() #Création d'un verrou pour les threads


# nmach = input('Nom machine à analyser : ')
# aIP = socket.gethostbyname(nmach)

aIP = input('Adresse IPv4 de la machine à analyser : ')

print ('Analyse de : ', aIP)

#Fonction qui analyse si le 'port' est ouvert sur 'aIP' (IP ou nom de machine)
def analport(port, nom) :
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Nouvel objet de connexion de type TCP en IPv4, paramètres par défaut pouvant être omis
  try :
    con = s.connect((aIP, port)) #Tente d'ouvrir une connexion avec pour paramètre un tuple(IPv4, port)
    with print_lock  : #Partie de programme qui sera exécutée thread par thread grâce au verrou print_lock
      print('%3d ouvert,  th(%d)' % (port, nom))
      con.close()
  except :
    pass



#**********************************
# Décalaration des classes
#**********************************


class ThreadPort(threading.Thread) :
  # Constructeur de l'objet
  def __init__(self, nom) :
    threading.Thread.__init__(self)
    self.nom = nom

  #Méthode d'un objet thread étant remplacée ici et invoquée par thread.start()
  def run(self) :
    while 1: #Il semble que ce soit plus efficace que 'while True:' en Python
      analport(q.get(), self.nom) #Passe à analport() le plus vieil élément restant de la pile de ports
      q.task_done() #L'élément passé précédément est enlevé de la pile, s'utilise après Queue.get()


#**********************************
# Fin de décalaration des classes
#**********************************


#**********************************
# Début du programme
#**********************************

q = Queue() #Crée une pile FIFO

top = time.time() #Enregistre l'heure de départ des traitements suivants

for x in range(100) : #Lancement de 100 threads
  t = ThreadPort(x) #Création d'un thread
  t.daemon = True #Permet au thread de mourir de sa belle mort plutôt que de hanter notre ordinateur
  t.start() #En voiture Simone


for noport in range(1, 2048+1) :
  q.put(noport) #Pousse un port dans la pile

q.join() #On attend que la pile soit vide pour continuer.
#Elle est utilisée dans la méthode run() de la classe Threadport().

print('Temps pris :', time.time() - top)

#**********************************
# Fin du programme
#**********************************
