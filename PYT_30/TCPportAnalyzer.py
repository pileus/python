#!/usr/bin/env python
# -*- coding: utf-8 -*-



import threading
from socket import *
from time import *

socket_lock = threading.Lock()  # Permet de ne pas mélanger les threads, utilisé dans ThreadAnalyzePort()


def open_port_analyzer(addr, start_port=1, stop_port=1024, step=5):
    """
    Instancie des objets ThreadAnalyzePort, les lance puis attend qu'ils aient finis.
    :param addr:
    :param start_port:
    :param stop_port:
    :param step:
    :return:
    """
    for i in range(start_port, stop_port, step):
        globals()[f"thrd{start_port}to{start_port + 5}"] = ThreadAnalyzePort(addr, i, step)
        # globals()[string] permet de créer un nom de variable à la volée ayant 'string' pour nom.
        globals()[f"thrd{start_port}to{start_port + 5}"].start()
    for i in range(start_port, stop_port, step):
        globals()[f"thrd{start_port}to{start_port + 5}"].join()


def test_one_port(addr, port):
    """
    Tente une connexion TCP, puis la ferme en cas de réussite.
    :param addr:
    :param port:
    :return:
    """
    sock8 = socket(AF_INET, SOCK_STREAM)
    sock8.settimeout(2)  # On considère qu'au bout de 2 secondes sans réponse la connexion est impossible.
    sock8.connect((addr, port))
    sock8.close()


class ThreadAnalyzePort(threading.Thread):
    """
    Crée des threads pour l'analyse de ports.
    """
    def __init__(self, addr, start_port, step):
        threading.Thread.__init__(self)
        self.addr = addr
        self.start_port = start_port
        self.step = step

    def run(self):
        for self.i in range(self.start_port, self.start_port+self.step):
            try:
                test_one_port(self.addr, self.i)
                pass
                with socket_lock:   # Permet de lancer un print thread par thread
                    # et pas tout le monde en même temps.
                    print(f"Le port {self.i} est ouvert.")
            except:
                pass
            finally:
                pass


def main():
    """The main function."""
    while 1:
        user_input = input("Saisir une addresse IPv4 ou un nom de machine ? Ou (Q)uitter.\n>>")
        if user_input.upper() == "Q":
            break
        addr = user_input
        user_input = input("Saisir la plage des ports à analyser ou (Q)uitter.\n>>")
        if user_input.upper() == "Q":
            break
        start_port = int(user_input.split(' ')[0])
        stop_port = int(user_input.split(' ')[1])
        temps = time()
        open_port_analyzer(addr, start_port, stop_port)
        temps = time() - temps
        print("That's all folks !!!")
        print(f"L'opération a pris {temps} secondes.")


if __name__ == "__main__":
    # main function call only if the current module
    # is not imported
    main()
