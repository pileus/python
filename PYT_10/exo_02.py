import re #Pour les expressions régulières

while True:
	Addr=input('Entrer une adresse IP au format CIDR :')
	#inAddr='195.5.7.12/8'
	if re.fullmatch(r'^(\d{1,3}\.){3}(\d{1,3})\/(\d{1,2})$',Addr) != None: #Format x.x.x.x/x
		#Séparation de l'adresse et du masque
		msk=int(Addr.split('/')[1])
		if not (0<msk<32):
			print("***   Le masque de sous-réseau doit être compris entre 1 et 31.   ***")
			continue
		#Séparation de chaque nombre de l'adresse IP
		strIP=Addr.split('/')[0].split('.')
		for i in range(0,4):
			strIP[i]=int(strIP[i])
		if (0<strIP[0]<=255) and (0<=strIP[1]<=255) and (0<=strIP[2]<=255) and (0<strIP[3]<255):
			break
		else:
			print("***   L'adresse n'est pas dans une plage valide.   ***")
			continue

	print("***   Le format n'est pas celui demandé.   ***")

# Prend l'octet frontière,
# le transforme en binaire,
# le sépare à l'endroit indiqué par le masque
# et retransforme en Int puis str
strIP[msk//8] = int(bin(strIP[msk//8])[2:].rjust(8,'0')[:msk%8].ljust(8,'0'),2)

#Remplacement des octets hosts par 0
for i in range(msk//8+1,4):
	strIP[i]=0

#Mise au format binaire du masque de sous-réseau
binMsk=''
for i in range(1,33):
	if i <= msk:
		binMsk+='1'
	else:
		binMsk+='0'
	if i==8 or i==16 or i==24:
		binMsk+='.'

#Transformation vers le format décimal
intMsk=binMsk.split('.')
for i in range(0,4):
	intMsk[i]=str(int(binMsk.split('.')[i],2))

for i in range(0,4):
	strIP[i]=str(strIP[i])

print('L\'adresse du réseau est : '+'.'.join(strIP))
print('Le masque de sous-réseau est : '+'.'.join(intMsk))
