"""
Ce programme permet d'afficher la table de multiplication d'un nombre,
dont on peut choisir le début et la fin, uniquement dans un ordre croissant.
"""
def mul(n,d,f):
	acc='{:'+str(len(str(f)))+'}' # Permet d'avoir un alignement des multiplicateurs
	# entre toutes les lignes de la table de multiplication
	# selon le nombre de caractères du plus long multiplicateur
	for i in range(d,f+1):
		print(('{} x '+acc+' = {}').format(n,i,n*i))
	pass

mul(23,95,105)